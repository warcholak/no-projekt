library(xlsx)
library(igraph)


# data load
input.prob <- as.data.frame(read.xlsx("C:/repozytorium/no-projekt/dane.xlsx", 1, header=TRUE))
input.val <- as.data.frame(read.xlsx("C:/repozytorium/no-projekt/dane.xlsx", 2, header=TRUE))
input.var <- as.data.frame(read.xlsx("C:/repozytorium/no-projekt/dane.xlsx", 3, header=TRUE))

Rmin <- input.var[1,2]
Kmax <- input.var[1,1]
rm(input.var)

input.prob <- input.prob[,names(input.prob)!='x']

###########################################

g <- graph_from_adjacency_matrix(adjmatrix = as.matrix(input.prob), mode = "directed"
                                 ,weighted = TRUE)

q.paths <- all_simple_paths(g, from = min(names(input.prob)), to = max(names(input.prob)), mode = c("out", "in", "all", "total"))

d <- c()
VP <- c()
EP <- c()
i <- 1
for (i in 1:length(q.paths)) { #iterate over edges to gather probability for paths
  VP <- q.paths[[i]]
  EP <- rep(VP, each=2)[-1]
  EP = EP[-length(EP)]
  d <- append(d, prod(E(g)$weight[get.edge.ids(g, EP)]))
}
rm(VP, EP, i)

ri <- runif(length(V(g)), 1.0, 1.0)

kr <- data.frame(ri, c(0))
colnames(kr) <- c("Ri", "Ki")
kr$Ki <- input.val$Si + input.val$ai*exp(input.val$bi*kr$Ri)
sum(kr$Ki)

f <- function(Ri){ #overall K
  Ki <- input.val$Si + input.val$ai*exp(input.val$bi*Ri)
  return(sum(Ki))
}

r.fun <- function(Ri) { #overall R
  i <- 1 
  R <- 0
  for (i in 1:length(q.paths)) {
    R <- R + d[i]*prod(Ri[rep(q.paths[[i]], each=1)])
    #print(paste("R: ", R, " ; ", d[i]*prod(Ri[rep(q.paths[[i]], each=1)])))
  }
  return(R)
}

 
while (f(kr$Ri) > Kmax && r.fun(kr$Ri) >= 0.99) { #first stop when cost criteria is satisfied (first occurence)
  #choose which Ri to be decreased basing on biggest K difference
  RiTemp <- kr$Ri
  RiTemp <- cbind(RiTemp, 0, 0)
  #RiTemp[,1] <- kr$Ri
  i <- 1#1
  for (i in 1:length(kr$Ki)) {
    if(i != 1 && i != length(kr$Ki)){
      RiTemp[i,1] <- RiTemp[i,1] - 0.001
      RiTemp[i,2] <- input.val$Si[i] + input.val$ai[i]*exp(input.val$bi[i]*RiTemp[i,1])
      RiTemp[i,3] <- kr$Ki[i] - RiTemp[i,2]
    }
  }
  vertex.id <- min(which(RiTemp[,3] == max(RiTemp[-c(1,length(kr$Ki)),3])))
  kr[vertex.id, 1] <- kr[vertex.id, 1] - 0.001
  kr$Ki <- input.val$Si + input.val$ai*exp(input.val$bi*kr$Ri)
}


while (r.fun(kr$Ri) >= 0.99) { #minimize cost until ovearll reliability reaches given limitation
 # print(paste("start ---- R: ", r.fun(kr$Ri), " ; ", "K: ", f(kr$Ri)))
  #choose which Ri to be decreased basing on biggest K difference
  RiTemp <- kr$Ri
  RiTemp <- cbind(RiTemp, 0, 0)
  #RiTemp[,1] <- kr$Ri
  i <- 1#1
  for (i in 1:length(kr$Ki)) {
    if(i != 1 && i != length(kr$Ki)){
      RiTemp[i,1] <- RiTemp[i,1] - 0.0001
      RiTemp[i,2] <- input.val$Si[i] + input.val$ai[i]*exp(input.val$bi[i]*RiTemp[i,1])
      RiTemp[i,3] <- kr$Ki[i] - RiTemp[i,2]
    }
  }
  R2Temp <- kr$Ri
  #print(R2Temp)
  vertex.id <- min(which(RiTemp[,3] == max(RiTemp[-c(1,length(kr$Ki)),3])))
  R2Temp[vertex.id] <- R2Temp[vertex.id] - 0.0001
  #print(R2Temp)
  if(r.fun(R2Temp) > 0.99){
   # print(paste("----if ---- R: ", r.fun(R2Temp)))
    kr$Ri[vertex.id] <- kr$Ri[vertex.id] - 0.0001
    kr$Ki <- input.val$Si + input.val$ai*exp(input.val$bi*kr$Ri)
  } else {
   # print(paste("----else ---- R: ", r.fun(R2Temp)))
    break()
  }
 # print(paste("end ---- R: ", r.fun(kr$Ri), " ; ", "K: ", f(kr$Ri)))
}

vertex.col <- kr$Ri
vertex.col[vertex.col != 1] <- "rosybrown3"
vertex.col[vertex.col == 1] <- "rosybrown1"

vertex.fr <- kr$Ri
vertex.fr[vertex.col != 1] <- "rosybrown3"
vertex.fr[vertex.col == 1] <- "rosybrown1"

tkplot(g, layout=layout_with_fr, edge.label=round(E(g)$weight, 2)
       ,vertex.label.dist=0, vertex.color=vertex.col, edge.color="rosybrown4", edge.width=2
       ,vertex.size = 25 , vertex.frame.color=vertex.fr, edge.arrow.size=1
       ,edge.label.color="black", edge.label.font=2, vertex.label.color="maroon"
       ,vertex.label.font=2)

cat(
  paste(
    "--Solution-------------------------------------------",
    paste("R: ", r.fun(kr$Ri), " ; ", "K: ", f(kr$Ri)),
    paste("Cost decreased by:", Kmax-f(kr$Ri)),
    paste("Reliability modified for: "),
    paste("  Vertex no:", which(kr$Ri != 1)),
    paste("-----------------------------------------------------"),
    sep= "\n"
  )
)



